<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserHomeController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('frontEnd.user.showLoginForm');
    }
    public function showAdForm()
    {
        return view('frontEnd.user.showAddForm');
    }
    public function storeAdForm(Request $request)
    {

    }
}
